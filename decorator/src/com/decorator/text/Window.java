package com.decorator.text;

public class Window {


    public static void main(String[] args) {
        Window w = new Window();
        // want a simple text view
        TextView tv = new TextView();
        //w.setContent(tv);

        // want a bordered text view
        //w.setContent(new com.decorator.text.BorderDecorator(tv,1));

        // want a bordered scrollable text view
        //w.setContent(new com.decorator.text.ScrollDecorator(new com.decorator.text.BorderDecorator(tv,1)));
        w.setContent(new ScrollDecorator(tv));

    }

    public void setContent(VisualComponent vc){
        vc.draw();
    }
}
