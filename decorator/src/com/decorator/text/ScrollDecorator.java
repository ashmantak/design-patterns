package com.decorator.text;

public class ScrollDecorator extends Decorator{

    public ScrollDecorator(VisualComponent component) {
        super(component);
    }

    public void draw(){
        super.getComponent().draw();
        scroll();
    }

    private void scroll(){
        System.out.println("add scroll");
    }
}
