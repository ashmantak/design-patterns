package com.decorator.text;

public class BorderDecorator extends Decorator{
    int width;

    public BorderDecorator(VisualComponent component, int borderWidth) {
        super(component);
        this.width=borderWidth;
    }

    public void draw(){
        super.getComponent().draw();
        drawBorder(width);
    }

    private void drawBorder(int width){
        System.out.println("draw border");
        // draw border with width
    }

}
