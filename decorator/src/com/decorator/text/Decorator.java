package com.decorator.text;

public class Decorator extends VisualComponent{

    private VisualComponent component;

    public Decorator(VisualComponent component){
        this.component=component;
    }

    public void draw(){
        component.draw();
    }

    public VisualComponent getComponent() {
        return component;
    }

    public void setComponent(VisualComponent component) {
        this.component = component;
    }
}
