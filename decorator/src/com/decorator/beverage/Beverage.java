package com.decorator.beverage;

public abstract class Beverage {

    private String name;
    private double unitPrice;
    private double quantity;

    public Beverage(String name, double unitPrice, double quantity) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice(){
        double price =  getUnitPrice()*getQuantity();
        System.out.println(this.name+" getPrice() = "+price);
        return price;
    }

}
