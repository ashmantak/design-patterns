package com.decorator.beverage.decorators;


import com.decorator.beverage.Beverage;

/*

A decorator IS-A Beverage and HAS-A Beverage

 */
public class SoyDecorator extends BeverageDecorator {
    public SoyDecorator(Beverage beverage, String name, double unitPrice, double quantity) {
        super(beverage, name, unitPrice, quantity);
    }

    public double getPrice(){
        double price =  super.getBeverage().getPrice()+ getUnitPrice()*getQuantity();
        System.out.println(" Beverage getPrice() after adding soy");
        return price;
    }

}
