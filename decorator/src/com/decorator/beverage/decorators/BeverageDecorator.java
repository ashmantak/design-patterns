package com.decorator.beverage.decorators;


import com.decorator.beverage.Beverage;

/*

A decorator IS-A Beverage and HAS-A Beverage

 */
public abstract class BeverageDecorator extends Beverage {
    private Beverage beverage;

    public BeverageDecorator(Beverage beverage, String name, double unitPrice, double quantity) {
        super(name, unitPrice, quantity);
        this.beverage = beverage;
    }

    public double getPrice(){
        System.out.println(" 123 Beverage getPrice()");
        return getUnitPrice()*getQuantity();
    }

    public Beverage getBeverage() {
        return beverage;
    }

    public void setBeverage(Beverage beverage) {
        this.beverage = beverage;
    }
}
