package com.decorator.beverage;

import com.decorator.beverage.concrete.Coffee;
import com.decorator.beverage.decorators.MilkDecorator;
import com.decorator.beverage.decorators.SoyDecorator;

public class Test {

    public static void main(String[] args) {
        // Build a coffee with milk and get price

        Coffee coffee = new Coffee("Coffee", 15,2);
        //System.out.println(coffee.getPrice());

        MilkDecorator milkDecorator = new MilkDecorator(coffee,"MILK",40,0.5);
        //System.out.println(milkDecorator.getPrice());

        SoyDecorator soyDecorator = new SoyDecorator(milkDecorator,"SOY", 10, 1);
        System.out.println(soyDecorator.getPrice());
    }
}
