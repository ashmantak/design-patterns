package com.decorator.beverage.concrete;

import com.decorator.beverage.Beverage;

public class Coffee extends Beverage {

    public Coffee(String name, double unitPrice, double quantity) {
        super(name, unitPrice, quantity);
    }
}
