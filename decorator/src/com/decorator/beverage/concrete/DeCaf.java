package com.decorator.beverage.concrete;

import com.decorator.beverage.Beverage;

public class DeCaf extends Beverage {

    public DeCaf(String name, double unitPrice, double quantity) {
        super(name, unitPrice, quantity);
    }
}
